import axios from 'axios';

export const getData = async () => {
    try {
        const response = await axios.get('https://restcountries.eu/rest/v2/all');
        return response.data.map(country => country.name);
    } catch (error) {
        throw error;
    }
};

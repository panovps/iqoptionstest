import { createStore, applyMiddleware, compose } from 'redux';
import reduxSaga from 'redux-saga';
import rootReducer from './reducers';
import rootSaga from './saga';

import CheckPlatform from './common/checkPlatform';

function configureStore(initialState) {
    const sagaMiddleware = reduxSaga();

    const initedComposeParams = [applyMiddleware(sagaMiddleware)];
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
        initedComposeParams.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    const store = createStore(
        rootReducer,
        initialState,
        compose.apply(this, initedComposeParams)
    );

    sagaMiddleware.run(rootSaga);
    return store;
}

export default configureStore({
    currentDevice: CheckPlatform()
});
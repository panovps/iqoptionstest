import React, { Component } from 'react';
import { connect } from 'react-redux';

import SuperSelect from '../components/SuperSelect/SuperSelect.jsx';
import { COUNTRIES_FETCH } from '../actions/';

const mapStateToProps = ({currentDevice, countries}) => {
    return {
        isMobile: currentDevice.isMobile,
        title: countries.title,
        data: countries.data,
        isPending: countries.isPending
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchFunc() {
            dispatch({ type: COUNTRIES_FETCH });
        }
    };
};

const mergeProps = (mapStates, dispatchProps, props) => {
    return {...mapStates, ...dispatchProps, ...props};
};


class ComponentPreloading extends Component {

    componentWillMount() {
        const {fetchFunc} = this.props;
        fetchFunc(this.props);
    }

    render() {
        return !this.props.isPending ? <SuperSelect {...this.props} /> : <div>Loading...</div>
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ComponentPreloading);
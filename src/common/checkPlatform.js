export default () => {
    const platformData = {
        android: /Android/i.test(navigator.userAgent),
        ios: /iPhone|iPad|iPod/i.test(navigator.userAgent),
        iPhone: /iPhone/i.test(navigator.userAgent),
        iPad: /iPad/i.test(navigator.userAgent)
    };
    platformData.isMobile = platformData.android || platformData.ios;

    return platformData;
};

import { COUNTRIES_FETCH, COUNTRIES_FETCH_SUCCESS, COUNTRIES_FETCH_FAIL } from '../actions';

const INITIAL_STATE = {
    title: 'Loading...',
    isPending: true,
    error: null,
    data: []
};

export default function (state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case COUNTRIES_FETCH:
            return {...state, isPending: true };

        case COUNTRIES_FETCH_SUCCESS:
            return {...state, data: action.data, isPending: false, title: 'Выберите страну...' };

        case COUNTRIES_FETCH_FAIL:
            return {...state, error: action.error, isPending: false };

        default: return state;
    }
}

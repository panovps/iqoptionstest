import { combineReducers } from 'redux';
import countries from './countriesReducer';

const rootReducer = combineReducers({
    currentDevice: (state = {}) => state,
    countries
});

export default rootReducer;

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';

import SuperSelectContainer from './containers/SuperSelectContainer.jsx';

class App extends Component {
    render() {
        return (<Provider store={store}>
            <div>
                <SuperSelectContainer onChange={value => alert(`Select 1: ${value}`)} />

                <div style={{position:'absolute', top: 'calc(60%)', left: 'calc(50% - 150px)'}}>
                    <SuperSelectContainer onChange={value => console.log('Select 2', value)} />
                </div>

                <div style={{position:'absolute', bottom: '5px', right: '5px'}}>
                    <SuperSelectContainer onChange={value => console.info('Select3', value)} />
                </div>

            </div>
        </Provider>);
    }
}

export default App;

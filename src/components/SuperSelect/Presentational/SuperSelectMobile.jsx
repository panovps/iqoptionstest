import React from 'react';
import PropTypes from 'prop-types';

const SelectMobil = ({data, onChange}) =>
    <select className="SuperSelect__selectMobile" onChange={evt => onChange(evt.target.value)}>
        {data.map(option => <option key={`Option ` + option}>{option}</option>)}
    </select>;

SelectMobil.PropTypes = {
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
};

export default SelectMobil;
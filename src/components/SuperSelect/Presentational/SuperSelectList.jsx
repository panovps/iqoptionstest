import React from 'react';
import PropTypes from 'prop-types';

function matchOption(option, search, onClick) {

    let match;

    if (search!=='' && option.toLowerCase().indexOf(search.toLowerCase())===0) {

        const regEx = new RegExp(search, "ig");
        match = <span>
            <span className="SuperSelect__item-active">{option.substring(0, search.length)}</span>
            {option.replace(regEx, '')}
        </span>

    } else {
        match = option;
    }

    return <li className="SuperSelect__item" key={`Option_` + option} onMouseDown={onClick}>{match}</li>

}

const SuperSelectList = ({orientation, expanded, searchData, search, onClick}) =>
    <ul className={`SuperSelect__list SuperSelect__list_${orientation}` + (expanded ? ` SuperSelect__list_expanded` : ``)}>
        {searchData.map(option => matchOption(option, search, () => onClick(option)))}
    </ul>;

SuperSelectList.PropTypes = {
    searchData: PropTypes.array.isRequired,
    orientation: PropTypes.string.isRequired,
    search: PropTypes.string,
    expanded: PropTypes.bool,
    onClick: PropTypes.func.isRequired
};

export default SuperSelectList;
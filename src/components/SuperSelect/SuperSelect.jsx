import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './SuperSelect.styl';

import SuperSelectMobile from './Presentational/SuperSelectMobile';
import SuperSelectList from './Presentational/SuperSelectList';

import debounce from 'lodash/debounce';

class SuperSelect extends Component {
    constructor(props) {
        super(props);

        const data = props.data.sort();

        this.state = {
            expanded: false,
            data,
            searchData: [...data],
            search: '',
            value: '',
            title: props.title || '',
            orientation: 'bottom',
            isMobile: props.isMobile
        };

        this.onChange = props.onChange;
    }

    toggleSelect = () => {
        this.setState(prevState => ({expanded: !prevState.expanded}));
    };

    typeInput = (evt) => {
        const search = evt.target.value;
        const searchData = this.state.data.filter(val => val.toLowerCase().indexOf(search.toLowerCase())===0);

        this.setState({ search, searchData });
    };

    setValue = (value) => {
        this.setState({
            value,
            search: '',
            searchData: [...this.state.data]
        });

        if (typeof this.onChange==='function') {
            this.onChange(value);
        }
    };

    checkListOrientation = prompt => {
        const promptY = prompt.getBoundingClientRect().y;
        const maxListHeight = 300;

        const checkBottom = window.innerHeight > promptY + maxListHeight;
        const isTopAvailable = promptY > maxListHeight;

        const orientation = !checkBottom && isTopAvailable ? 'top' : 'bottom';

        if (this.state.orientation !== orientation) {
            this.setState({ orientation });
        }
    };

    componentDidMount () {
        this.onResizeWindow = debounce(() => {
            this.checkListOrientation(this.promptElment);
        }, 200);

        this.onResizeWindow();

        window.addEventListener('resize', this.onResizeWindow );
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.onResizeWindow);
    }

    render() {

        const List = this.props.isMobile
                ? <SuperSelectMobile data={this.state.data} onChange={this.setValue} />
                : <SuperSelectList {...this.state} onClick={this.setValue} />;

        return (
            <div className="SuperSelect">

                <div className={`SuperSelect__prompt` + (this.state.expanded ? ` SuperSelect__prompt_${this.state.orientation}` : '')}
                    onClick={() => !this.state.expanded && !this.props.isMobile && this.toggleSelect()}
                    ref={prompt => this.promptElment = prompt}>

                    <div className={`SuperSelect__box` + (this.state.expanded ? ` SuperSelect__box_expanded` : ``)}>
                        {this.state.expanded ? this.state.title : this.state.value || this.state.title}
                    </div>

                    <button className={`SuperSelect__switch` + (this.state.expanded ? ` SuperSelect__switch_expanded` : ``)}
                        dangerouslySetInnerHTML={{__html: this.state.expanded ? `&#9652;` : `&#9662;`}}
                    />

                    {this.state.expanded && !this.props.isMobile &&
                        <input ref={input => input && input.focus()}
                                className="SuperSelect__input" type="text"
                                onInput={this.typeInput}
                                onBlur={this.toggleSelect}
                                value={this.state.search}
                    />}

                    { List }

                </div>

            </div>
        );
    }
}

SuperSelect.propTypes = {
  data: PropTypes.array.isRequired,
  title: PropTypes.string,
  isMobile: PropTypes.bool,
  onChange: PropTypes.func
};

export default SuperSelect;

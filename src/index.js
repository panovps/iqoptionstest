import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const dom = document.createElement('div');
document.body.appendChild(dom);

ReactDOM.render(<App />, dom);
import { fork } from 'redux-saga/effects';

import * as CountriesSaga from './fetchCountries';

export default function* rootSaga() {
    yield [
        fork(CountriesSaga.initSaga)
    ];
}

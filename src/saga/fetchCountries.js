import { take, put, call, fork } from 'redux-saga/effects';
import * as api from '../api/fetchCountries';
import { COUNTRIES_FETCH, COUNTRIES_FETCH_SUCCESS, COUNTRIES_FETCH_FAIL } from '../actions/';

export function* fetchData() {
    try {
        while (true) {
            yield take(COUNTRIES_FETCH);
            const response = yield call(api.getData);
            yield put({ type: COUNTRIES_FETCH_SUCCESS, data: response });
        }
    } catch (error) {
        console.error(error);
        yield put({ type: COUNTRIES_FETCH_FAIL, error });
    }
}

export function* initSaga() {
    yield [
        fork(fetchData)
    ];
}
